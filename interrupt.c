/*
** interrupt.c for BeagleK
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Tue Aug  6 12:50:30 2013 Pierre Surply
** Last update Sun Aug 11 11:45:40 2013 Pierre Surply
*/

#include <stdlib.h>
#include <stdio.h>
#include "interrupt.h"
#include "uart.h"
#include "timer.h"
#include "rammap.h"
#include "userland/syscall.h"

void set_ilr(volatile uint32_t *ilr, uint8_t priority, bool fiq)
{
        *ilr = ((priority & 0x3F) << 2) | (fiq & 1);
}

void unmask(uint8_t i)
{
        if (i < 32)
                INTC_MIR0_CLEAR = 1 << i;
        else if (i < 64)
                INTC_MIR1_CLEAR = 1 << (i - 32);
        else if (i < 96)
                INTC_MIR2_CLEAR = 1 << (i - 64);
        else
                INTC_MIR3_CLEAR = 1 << (i - 96);
}

void interrupt_init(void)
{
        unmask(68);
}

void do_data_abort(void)
{
        printf("Data abort...\n\r");
        for (;;);
}

void *do_software_interrupt(uint32_t id, void *arg0,    \
                            void *arg1, void *arg2)
{
        switch (id)
        {
        case SYSCALL_WRITE:
                return (void *) syscall_write((char *) arg0, (size_t) arg1);
        case SYSCALL_READ:
                return (void *) syscall_read((char *) arg0, (size_t) arg1);
        default:
                printf("Syscall %d %x %x %x \n\r", id, arg0, arg1, arg2);
                return NULL;
        }
}

void do_irq(void)
{
        switch (INTC_SIR_IRQ)
        {
        case 68:
                timer_irq();
                break;
        default:
                printf("IRQ : %d\n\r", INTC_SIR_IRQ);
                break;
        }
        INTC_CONTROL = NewIRQAgr | NewFIQAgr;
}
