/*
** timer.c for BeagleK
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Fri Jul 26 14:25:56 2013 Pierre Surply
** Last update Sat Aug 10 14:23:59 2013 Pierre Surply
*/

#include <stdint.h>
#include <stdio.h>
#include "timer.h"
#include "gpio.h"

void timer_write(uint32_t counter)
{
        DMTIMER1_TCRR = counter;
}

uint32_t timer_read(void)
{
        return DMTIMER1_TCRR;
}

void timer_set_match(uint32_t match)
{
        DMTIMER1_TMAR = match;
}

void timer_init(void)
{
        timer_write(0);
        DMTIMER1_IRQENABLE_SET = MAT_FLAG;
        DMTIMER1_TCLR = CE | PRE | PTV1 | PTV2 | PTV3 | AR | ST;
}

inline void timer_irq(void)
{
        if (DMTIMER1_IRQSTATUS & MAT_FLAG)
        {
                update_leds();
                DMTIMER1_IRQSTATUS = MAT_FLAG;
                timer_write(0);
        }
        else if (DMTIMER1_IRQSTATUS & OVF_FLAG)
                DMTIMER1_IRQSTATUS = OVF_FLAG;
}
