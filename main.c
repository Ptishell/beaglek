/*
** main.c for BeagleK
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Wed Jul 24 10:15:02 2013 Pierre Surply
** Last update Tue Aug 13 11:04:23 2013 Pierre Surply
*/

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include "interrupt.h"
#include "uart.h"
#include "gpio.h"
#include "timer.h"
#include "rammap.h"

#define LED_SPEED       5000

int main(void);

void jump_to_userland(void)
{
        asm volatile("ldr     r0, =0x00000050"          "\n\t"
                     "msr     cpsr, r0"                 "\n\t"
                     "ldr     sp, =begin_user_stack"    "\n\t"
                     ::);
        main();
        for(;;);
}

int bkmain(void)
{
        uart_init();
        interrupt_init();

        gpio_set_direction(USR0, GPIO_OUT);
        gpio_set_direction(USR1, GPIO_OUT);
        gpio_set_direction(USR2, GPIO_OUT);
        gpio_set_direction(USR3, GPIO_OUT);

        timer_set_match(LED_SPEED);
        timer_init();

        printf("*** BeagleK ***\r\n");
        printf("Text\t\t%x\n\r", (uint32_t) begin_text);
        printf("Rodata\t\t%x\n\r", (uint32_t) begin_rodata);
        printf("Data\t\t%x\n\r", (uint32_t) begin_data);
        printf("BSS\t\t%x\n\r", (uint32_t) begin_bss);
        printf("Heap\t\t%x\n\r", (uint32_t) begin_heap);
        printf("Stack\t\t%x\n\r", (uint32_t) begin_stack);
        printf("IRQ Stack\t%x\n\r", (uint32_t) begin_irq_stack);

        printf("Starting userland program...\n\r");
        jump_to_userland();

        return 0;
}
