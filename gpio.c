/*
** gpio.c for BeagleK
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Thu Jul 25 12:29:25 2013 Pierre Surply
** Last update Thu Aug  8 11:44:37 2013 Pierre Surply
*/

#include <stdint.h>
#include <stdio.h>
#include "gpio.h"

void gpio_set_direction(short pin, uint8_t direction)
{
        if (direction)
                GPIO1_DIRECTION |= 1 << pin;
        else
                GPIO1_DIRECTION &= ~(1 << pin);
}

void gpio_out(short pin, bool state)
{
        if (state)
                GPIO1_SETDATAOUT = 1 << pin;
        else
                GPIO1_CLEARDATAOUT = 1 << pin;
}

void update_leds(void)
{
        static uint8_t  i = 0;
        static bool     s = false;

        gpio_out(USR0, false);
        gpio_out(USR1, false);
        gpio_out(USR2, false);
        gpio_out(USR3, false);
        gpio_out(21 + i % 4, true);
        if (s && ++i > 3)
        {
                s = false;
                i = 2;
        }
        else if (!s && i-- == 0)
        {
                s = true;
                i = 1;
        }
}
