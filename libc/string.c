/*
** string.c for BeagleK
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Sun Aug  4 18:01:58 2013 Pierre Surply
** Last update Mon Aug  5 13:25:43 2013 Pierre Surply
*/

#include <string.h>

void*   memcpy(void *dest, const void *src, size_t n)
{
        for (; n > 0; --n)
                ((char *)dest)[n - 1] = ((char *)src)[n - 1];
        return dest;
}
