/*
** string.h for BeagleK
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Sun Aug  4 18:00:03 2013 Pierre Surply
** Last update Sun Aug 11 11:35:39 2013 Pierre Surply
*/

#ifndef _STRING_H_
#define _STRING_H_

#include <stdlib.h>

void    *memcpy(void *dest, const void *src, size_t n);

#endif /* _STRING_H_ */
