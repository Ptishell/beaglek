/*
** stdarg.h for BeagleK
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Mon Aug  5 17:09:28 2013 Pierre Surply
** Last update Mon Aug  5 17:15:47 2013 Pierre Surply
*/

#ifndef _STDARG_H_
#define _STDARG_H_

#include <stdlib.h>

typedef void*   va_list;

#define ALIGN(type)             \
        (sizeof (type) +        \
         (sizeof (type) - (sizeof (type) % sizeof (size_t))) % sizeof (size_t))

#define va_start(ap, lastarg)   \
        (ap = (char *) &(lastarg) + ALIGN(lastarg))

#define va_arg(ap, type)        \
        (ap += ALIGN(type), *((type *) (ap - ALIGN(type))))

#define va_end(ap)              \
        ;

#endif /* _STDARG_H_ */
