/*
** stdio.h for BeagleK
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Mon Aug  5 17:06:24 2013 Pierre Surply
** Last update Mon Aug  5 17:07:13 2013 Pierre Surply
*/

#ifndef _STDIO_H_
#define _STDIO_H_

int printf(const char *fmt, ...);

#endif /* _STDIO_H_ */
