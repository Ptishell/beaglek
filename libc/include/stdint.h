/*
** stdint.h for BeagleK
** 
** Made by Pierre Surply
** <pierre.surply@gmail.com>
** 
** Started on  Sun Aug  4 17:49:31 2013 Pierre Surply
** Last update Sun Aug  4 17:51:45 2013 Pierre Surply
*/

#ifndef _STDINT_H_
#define _STDINT_H_

typedef signed char		int8_t;
typedef unsigned char		uint8_t;
typedef short int		int16_t;
typedef unsigned short int	uint16_t;
typedef int			int32_t;
typedef unsigned int		uint32_t;

#endif /* _STDINT_H_ */
