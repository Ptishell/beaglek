/*
** stdlib.h for BeagleK
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Sun Aug  4 18:13:18 2013 Pierre Surply
** Last update Sat Aug 10 17:31:14 2013 Pierre Surply
*/

#ifndef _STDLIB_H_
#define _STDLIB_H_

#define NULL    ((void *) 0)

typedef unsigned int    size_t;

void *malloc(size_t size);
void free(void *data);

#endif /* _STDLIB_H_ */
