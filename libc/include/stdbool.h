/*
** stdbool.h for BeagleK
** 
** Made by Pierre Surply
** <pierre.surply@gmail.com>
** 
** Started on  Sun Aug  4 17:44:41 2013 Pierre Surply
** Last update Sun Aug  4 17:49:16 2013 Pierre Surply
*/

#ifndef _STDBOOL_H_
#define _STDBOOL_H_

#define true	1
#define false	0

typedef unsigned char	bool;

#endif /* _STDBOOL_H_ */
