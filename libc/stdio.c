/*
** stdio.c for BeagleK
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Mon Aug  5 16:54:03 2013 Pierre Surply
** Last update Mon Aug  5 17:18:29 2013 Pierre Surply
*/

#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>

#include "../uart.h"

int vprintf(const char *fmt, va_list args)
{
        for (; *fmt; ++fmt)
        {
                if (*fmt == '%')
                {
                        switch(*(++fmt))
                        {
                        case 'd':
                                uart_sendd(va_arg(args, uint32_t));
                                break;
                        case 'c':
                                uart_sendc(va_arg(args, char));
                                break;
                        case 'x':
                                uart_sendx(va_arg(args, uint32_t));
                                break;
                        case 's':
                                uart_sends(va_arg(args, char*));
                                break;
                        default:
                                return 1;
                                break;
                        }
                }
                else
                        uart_sendc(*fmt);
        }

        return 0;
}

int printf(const char *fmt, ...)
{
        int     res;
        va_list args;

        va_start(args, fmt);
        res = vprintf(fmt, args);
        va_end(args);
        return res;
}
