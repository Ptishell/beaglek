/*
** malloc.c for BeagleK
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Mon Aug  5 17:20:03 2013 Pierre Surply
** Last update Sat Aug 10 16:57:18 2013 Pierre Surply
*/

#include <stdlib.h>
#include "../rammap.h"

static void     *brk = begin_heap;

void *malloc(size_t size)
{
        void *tmp = brk;
        brk = (char *) brk + size;
        return tmp;
}

void free(void *data)
{
        data = NULL;
}
