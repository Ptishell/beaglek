# BeagleK

A [BeagleBone Black][1] bare-metal programming project including :

  - UART
  - GPIO
  - Timers
  - Exceptions
  - IRQ
  - Syscall

![BeagleBone][3]

## Dependencies

### Software

  - arm-none-eabi-binutils
  - arm-none-eabi-gcc
  - A tftp server
  - A serial communication program (minicom, ...)

### Hardware

  - BeagleBone Black
  - USB-TTL cable
  - RJ-45 cable

## Executing BeagleK

Compiling the project :

    $ make

Make sure that the file `beaglek.bin` is successfully generated after compiling
and put it in your tftp server.

Connect your USB-TTL cable to the BeagleBone serial debug pins to get the
u-boot command line (115200 8N1).

    U-Boot# setenv serverip yourtftpserverip
    U-Boot# dhcp
    U-Boot# tftp 0x80200000 beaglek.bin
    U-Boot# go 0x80200000

## Using the modified u-boot

A modified version of the u-boot `am335x_evm` configuration is also available in
this repository. It provides the following env settings :

    autoload=no
    bootdelay=10
    serverip=192.168.8.1
    ipaddr=192.168.8.2
    netmask=255.255.255.0
    bootk=tftp 0x80200000 beaglek.bin; go 0x80200000

Download [u-boot source code][2] and replace the configuration file :

    $ cp /path/to/beaglek/u-boot/include/configs/am335x_evm.h \
      /path/to/u-boot/include/configs/am335x_evm.h
    $ cd /path/to/u-boot/
    $ make ARCH=arm am335x_evm_config
    $ make ARCH=arm

Then, replace the generated files `u-boot.img` and `MLO` on the boot partition
of the eMMC (`/dev/mmcblk0p1`).

If your tftp server is at `192.168.8.1`, BeagleK can be loaded and
executed with the following command :

    U-Boot# run bootk

[1]: http://beagleboard.org/
[2]: http://www.denx.de/wiki/U-Boot/SourceCode
[3]: http://www.psurply.com/beaglek/beaglebone.jpg
