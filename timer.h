/*
** timer.h for BeagleK
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Fri Jul 26 14:28:09 2013 Pierre Surply
** Last update Sat Aug 10 14:24:57 2013 Pierre Surply
*/

#ifndef _TIMER_H_
#define _TIMER_H_

#include <stdint.h>

#define DMTIMER0        0x44E05000
#define DMTIMER1        0x48040000

#define DTIMER1_ILR             (*(volatile uint32_t *)0x48200100 + (66 * 4))

#define DMTIMER1_IRQSTATUS_RAW  (*(volatile uint32_t *)0x48040024)
#define DMTIMER1_IRQSTATUS      (*(volatile uint32_t *)0x48040028)
#define DMTIMER1_IRQENABLE_SET  (*(volatile uint32_t *)0x4804002C)
#define MAT_FLAG                (1 << 0)
#define OVF_FLAG                (1 << 1)

#define DMTIMER1_TCLR   (*(volatile uint32_t *)0x48040038)
#define ST              (1 << 0)
#define AR              (1 << 1)
#define PTV1            (1 << 2)
#define PTV2            (1 << 3)
#define PTV3            (1 << 4)
#define PRE             (1 << 5)
#define CE              (1 << 6)

#define DMTIMER1_TCRR   (*(volatile uint32_t *)0x4804003C)
#define DMTIMER1_TMAR   (*(volatile uint32_t *)0x4804004C)

void            timer_init(void);
void            timer_irq(void);
uint32_t        timer_read(void);
void            timer_write(uint32_t counter);
void            timer_set_match(uint32_t match);

#endif /* _TIMER_H_ */
