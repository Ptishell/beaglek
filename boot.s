/*
** boot.s for BeagleK
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Wed Jul 24 14:38:51 2013 Pierre Surply
** Last update Sun Aug 11 11:50:24 2013 Pierre Surply
*/


        .global _start
_start:
        b       reset
        ldr     pc, _undefined_instruction
        ldr     pc, _software_interrupt
        ldr     pc, _prefetch_abort
        ldr     pc, _data_abort
        ldr     pc, _not_used
        ldr     pc, _irq
        ldr     pc, _fiq

_undefined_instruction:
        .word undefined_instruction
_software_interrupt:
        .word software_interrupt
_prefetch_abort:
        .word prefetch_abort
_data_abort:
        .word data_abort
_not_used:
        .word not_used
_irq:
        .word irq
_fiq:
        .word fiq

reset:
        ldr     sp, =begin_stack
        ldr     r0, =0x00000053
        msr     cpsr, r0
        ldr     r0, =_start
        mcr     p15, 0, r0, c12, c0, 0
        bl      bkmain
        b       .

       .align   5
undefined_instruction:
        b       .

       .align   5
software_interrupt:
        stmfd   sp!, {r1-r12, lr}
        msr     cpsr, r0
        bl      do_software_interrupt
        ldmfd   sp!, {r1-r12, lr}
        subs    pc, lr, #0

       .align   5
prefetch_abort:
        b       .

        .align   5
data_abort:
        bl      do_data_abort
        b       .

        .align   5
not_used:
        b       .

        .align   5
irq:
        ldr     sp, =begin_irq_stack
        stmfd   sp!, {r0-r12, lr}
        mrs     r11, spsr
        bl      do_irq
        msr     spsr, r11
        ldmfd   sp!, {r0-r12, lr}
        subs    pc, lr, #4

        .align   5
fiq:
        b       .
