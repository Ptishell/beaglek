##
## Makefile for BeagleK
##
## Made by Pierre Surply
## <pierre.surply@gmail.com>
##
## Started on  Wed Jul 24 10:10:24 2013 Pierre Surply
## Last update Sat Aug 10 18:54:37 2013 Pierre Surply
##

EXEC	= beaglek
IFACE	= eth0
TFTPDIR = /srv/tftp/
PREFIX	= arm-none-eabi-

BIN	= $(EXEC).bin
ELF	= $(EXEC).elf
LIST	= $(EXEC).list

CC	= $(PREFIX)gcc
CFLAGS	= -Wall -Werror -nostdinc -nostdlib		\
	  -fno-builtin -nostartfiles -ffreestanding	\
	  -I ./libc/include

AS	= $(PREFIX)as
AFLAGS	= --warn --fatal-warnings

LD	= $(PREFIX)ld

OBJCOPY	= $(PREFIX)objcopy
OFLAGS	= -O binary

OBJDUMP	= $(PREFIX)objdump

SRC = main.c interrupt.c uart.c gpio.c timer.c
OBJ = boot.o $(SRC:.c=.o)

STDSRC	= $(wildcard ./libc/*.c)
STDOBJ	= $(STDSRC:.c=.o)

USRSRC	= $(wildcard ./userland/*.c)
USROBJ	= $(USRSRC:.c=.o)

all: $(BIN)

$(BIN): $(OBJ) linker.ld $(STDSRC) $(USRSRC)
	$(MAKE) -C ./libc CC=$(CC)
	$(MAKE) -C ./userland CC=$(CC)
	$(LD) -T linker.ld $(OBJ) $(STDOBJ) $(USROBJ) -o $(ELF)
	$(OBJDUMP) -D $(ELF) > $(LIST)
	$(OBJCOPY) $(ELF) $(OFLAGS) $@

boot.o: boot.s
	$(AS) $< -o $@

test: $(BIN)
	cp $(BIN) $(TFTPDIR)
	ifconfig $(IFACE) 192.168.8.1 netmask 255.255.255.0 up

clean:
	$(MAKE) -C ./libc clean
	rm -rf *.o *.bin *.list *.elf
