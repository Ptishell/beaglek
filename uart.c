/*
** uart.c for BeagleK
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Wed Jul 24 15:55:34 2013 Pierre Surply
** Last update Sun Aug 11 11:51:27 2013 Pierre Surply
*/

#include <stdint.h>
#include <stdlib.h>

#include "uart.h"

void uart_init(void)
{
        UART0_IER = RHRIT;
}

void uart_sendc(char c)
{
        while (!(UART0_LSR & TXFIFOE));
        UART0_THR = c;
}

void uart_sends(const char *s)
{
        for (; *s; ++s)
                uart_sendc(*s);
}

void uart_sendx(uint32_t x)
{
        const char      c[] = "0123456789ABCDEF";
        uint8_t         i;

        uart_sends("0x");
        for (i = 8; i > 0; --i)
                uart_sendc(c[(x >> ((i - 1) * 4)) & 0xF]);
}

void uart_sendd(int32_t x)
{
        char            buffer[256];
        uint8_t         i;

        i = 0;

        if (x < 0)
        {
                uart_sendc('-');
                x = -x;
        }
        do {
                buffer[i++] = (x % 10) + '0';
        } while ((x /= 10) > 0);

        for (; i > 0; --i)
                uart_sendc(buffer[i - 1]);
}

inline bool uart_available(void)
{
        return (!(UART0_LSR & RXFIFOE));
}

char uart_recv(void)
{
        while (!(UART0_LSR & RXFIFOE));
        return UART0_RHR;
}


int syscall_write(char *s, size_t len)
{
        size_t  i;

        for (i = 0; i < len; ++i)
                uart_sendc(s[i]);
        return len;
}

int syscall_read(char *s, size_t len)
{
        size_t  i;
        uint8_t x;

        for (i = 0; i < len; ++i)
        {
                if (!uart_available())
                        return -1;
                uart_sendc(x = uart_recv());
                if (x == '\r')
                {
                        s[i] = 0;
                        uart_sendc('\n');
                        return i;
                }
                else
                        s[i] = x;
        }
        return len;
}
