/*
** rammap.h for BeagleK
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Mon Aug  5 13:34:43 2013 Pierre Surply
** Last update Sat Aug 10 14:20:07 2013 Pierre Surply
*/

#ifndef _RAMMAP_H_
#define _RAMMAP_H_

extern char     begin_text[];
extern char     begin_rodata[];
extern char     begin_data[];
extern char     begin_bss[];
extern char     begin_heap[];
extern char     begin_stack[];
extern char     begin_irq_stack[];

#endif /* _RAMMAP_H_ */
