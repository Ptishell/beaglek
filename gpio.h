/*
** gpio.h for BeagleK
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Thu Jul 25 12:30:47 2013 Pierre Surply
** Last update Tue Aug  6 10:33:15 2013 Pierre Surply
*/

#ifndef _GPIO_H_
#define _GPIO_H_

#include <stdbool.h>
#include <stdint.h>

#define GPIO0                   0x44E07000
#define GPIO1                   0x4804C000
#define GPIO2                   0x481AC000
#define GPIO3                   0x481AE000

#define GPIO1_DIRECTION         (*(volatile uint32_t *)0x4804C134)
#define GPIO1_CLEARDATAOUT      (*(volatile uint32_t *)0x4804C190)
#define GPIO1_SETDATAOUT        (*(volatile uint32_t *)0x4804C194)

#define USR0                    21
#define USR1                    22
#define USR2                    23
#define USR3                    24

#define GPIO_OUT                0
#define GPIO_IN                 1

void gpio_set_direction(short pin, uint8_t direction);
void gpio_out(short pin, bool state);
void update_leds(void);

#endif /* _GPIO_H_ */
