/*
** uart.h for BeagleK
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Wed Jul 24 15:51:16 2013 Pierre Surply
** Last update Tue Aug 13 10:58:02 2013 Pierre Surply
*/

#ifndef _UART_H_
#define _UART_H_

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#define UART0_ILR       (*(volatile uint32_t *)(0x48200100 + 72))

#define UART0_THR       (*(volatile uint16_t *)0x44E09000)
#define UART0_RHR       (*(volatile uint16_t *)0x44E09000)

#define UART0_IER       (*(volatile uint16_t *)0x44E09004)
#define THRIT           (1 << 1)
#define RHRIT           (1 << 0)

#define UART0_LSR       (*(volatile uint16_t *)0x44E09014)
#define TXFIFOE         (1 << 5)
#define RXFIFOE         (1 << 0)

void uart_init(void);
void uart_sendc(char c);
void uart_sends(const char *c);
void uart_sendx(uint32_t x);
void uart_sendd(int32_t x);

bool uart_available(void);
char uart_recv(void);

int syscall_write(char *s, size_t len);
int syscall_read(char *s, size_t len);

#endif /* UART_H_ */
