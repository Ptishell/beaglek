/*
** syscall.c for BeagleK
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Sat Aug 10 18:42:38 2013 Pierre Surply
** Last update Sun Aug 11 10:48:16 2013 Pierre Surply
*/

#include <stdlib.h>
#include "syscall.h"

void *syscall(uint32_t id, void *arg1,    \
              void *arg2, void *arg3)
{
        void    *res;
        asm volatile ("push {lr}"       "\n\t"
                      "svc #0"          "\n\t"
                      "pop {lr}"        "\n\t"
                      "mov %0, r0"      "\n\t"
                      : "=r" (res)
                      :);
        return res;
}
