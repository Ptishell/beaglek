/*
** main.c for BeagleK
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Sat Aug 10 19:04:27 2013 Pierre Surply
** Last update Sun Aug 11 11:52:41 2013 Pierre Surply
*/

#include <bkstd.h>

int main(void)
{
        char            buff[256];
        unsigned int    len;

        write("Hello World !\n\r", 15);

        for (;;)
        {
                write("> ", 4);
                while ((len = read(buff, sizeof (buff))) < 0);
                write(buff, len);
                if (len > 0)
                        write("\n\r", 2);
        }

        return 0;
}
