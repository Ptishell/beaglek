/*
** bkstd.c for BeagleK
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Sat Aug 10 18:45:17 2013 Pierre Surply
** Last update Sun Aug 11 11:15:50 2013 Pierre Surply
*/

#include <stdlib.h>
#include "bkstd.h"
#include "syscall.h"

int write(char *s, size_t len)
{
        return (int) syscall(SYSCALL_WRITE, s, (void *) len, 0);
}

int read(char *s, size_t len)
{
        return (int) syscall(SYSCALL_READ, s, (void *) len, 0);
}
