/*
** bkstd.h for BeagleK
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Sat Aug 10 18:43:33 2013 Pierre Surply
** Last update Sun Aug 11 11:23:28 2013 Pierre Surply
*/

#ifndef _BKSTD_H_
#define _BKSTD_H_

#include <stdlib.h>

int write(char *s, size_t len);
int read(char *s, size_t len);

#endif /* _BKSTD_H_ */
