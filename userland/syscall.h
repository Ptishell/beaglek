/*
** syscall.h for BeagleK
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Sat Aug 10 18:38:27 2013 Pierre Surply
** Last update Sun Aug 11 10:42:18 2013 Pierre Surply
*/

#ifndef _SYSCALL_H_
#define _SYSCALL_H_

#include <stdint.h>

#define SYSCALL_WRITE   1
#define SYSCALL_READ    2

void *syscall(uint32_t id, void *arg1,  \
              void *arg2, void *arg3);

#endif /* _SYSCALL_H_ */
