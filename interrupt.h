/*
** interrupt.h for BeagleK
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Tue Aug  6 13:48:27 2013 Pierre Surply
** Last update Thu Aug  8 14:33:17 2013 Pierre Surply
*/

#ifndef _INTERRUPT_H_
#define _INTERRUPT_H_

#define INTC_SYSCONTROL         (*(volatile uint32_t *)(0x48200010))
#define AutoIdle                (1 << 0)

#define INTC_SIR_IRQ            (*(volatile uint32_t *)(0x48200040))

#define INTC_CONTROL            (*(volatile uint32_t *)(0x48200048))
#define NewIRQAgr               (1 << 0)
#define NewFIQAgr               (1 << 1)

#define INTC_IDLE               (*(volatile uint32_t *)(0x48200050))
#define FuncIdle                (1 << 0)
#define Turbo                   (1 << 1)

#define INTC_MIR0               (*(volatile uint32_t *)(0x48200084))
#define INTC_MIR0_CLEAR         (*(volatile uint32_t *)(0x48200088))
#define INTC_MIR1               (*(volatile uint32_t *)(0x482000A4))
#define INTC_MIR1_CLEAR         (*(volatile uint32_t *)(0x482000A8))
#define INTC_MIR2               (*(volatile uint32_t *)(0x482000C4))
#define INTC_MIR2_CLEAR         (*(volatile uint32_t *)(0x482000C8))
#define INTC_MIR3               (*(volatile uint32_t *)(0x482000E4))
#define INTC_MIR3_CLEAR         (*(volatile uint32_t *)(0x482000E8))

#define INTC_THRESHOLD          (*(volatile uint32_t *)(0x48200068))

void interrupt_init(void);

#endif /* _INTERRUPT_H_ */
